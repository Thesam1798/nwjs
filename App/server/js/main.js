const win = nw.Window.get();

win.setAlwaysOnTop(true);

//Vérification de la transparence
if (win.isTransparent === false) {
    window.location = "./error.html";
    exit();
}

//Lecture du fichier des parametres
require('fs').readFile((require("path").join(require("path").resolve("./App/"), 'config.json')), {encoding: 'utf-8'}, function (err, data) {
        if (!err) {

            data = JSON.parse(data);

            //Ajout de l'IP dans le menu
            if (document.getElementById('app-ip') !== null && data.secu.ipmask === false) {
                document.getElementById('app-ip').textContent = require("ip").address() + ":" + 47000;
            }else{
                if(document.getElementById('app-ip') !== null){
                    document.getElementById('app-ip').textContent = "IP masqué";
                }
            }

        } else {
            console.log(err);
        }
    }
);


const path = require("path"),
    fs = require('fs'),
    ip = require('ip'),
    md5 = require('md5'),
    http = require('http');

//Attente de jquery
$(document).ready(function () {

    $('#notif').hide();

    //Lecture du fichier des parametres
    filePath = require("path").join(require("path").resolve("./App/"), 'config.json');
    require('fs').readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
            if (!err) {
                App(JSON.parse(data));
            } else {
                console.log(err);
            }
        }
    );

});

function App(data) {
    index_page(data.secu.login);
    socker_io(start_server(), data.secu.password, data.affich.text, data.affich.shadow, data.affich.pos);
}

//Lecture du fichier de base et écriture du fichier client
function index_page(login) {
    let filePath = path.join(path.resolve("./App/client/assets/js/app"), 'appclient.js');
    fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
        if (!err) {

            fs.writeFile(path.resolve("./App/client/") + "/appclient.js", data.replace("IP-DU-SERVER", ip.address() + ":" + 47000), function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log("The file application.js was saved!");
            });

        } else {
            console.log(err);
        }
    });
}


function start_server() {
    // Chargement du fichier index.html affiché au client
    return http.createServer(function (req, res) {
        //Vérification de l'URL /
        if (req.url === '/') {
            fs.readFile(path.resolve("./App/client/") + "/index.html", 'utf-8', function (error, content) {
                res.end(content);
            });
        } else {
            //Si non appelle du fichier
            fs.readFile(path.resolve("./App/client/") + req.url, 'utf-8', function (error, content) {
                res.end(content);
            });
        }
    });
}

function socker_io(server, password, texte_color, bg_color, pos) {

    // Chargement de socket.io
    const io = require('socket.io').listen(server);

    // Quand un client se connecte
    io.sockets.on('connection', function (socket) {
        console.log("Client : " + socket.id + " est connecté !");

        let me;

        socket.on('login', function (user) {
            me = user;
            me.id = md5(user.username);

            if (md5(user.password) === md5(password)) {

                socket.emit('loginOk', {
                    username: user.username,
                    id: me.id
                });
                $('#notif').removeClass('alert-success').removeClass('alert-danger').addClass('alert-success').show();
                $('#notif_strong').html("Nouvelle connexion");
                $('#notif_text').html("L'utilisateur : " + user.username + ", c'est connecter");

                setTimeout(function () {
                    $('#notif').removeClass('alert-success').removeClass('alert-danger').hide();
                }, 4000);

            } else {
                socket.emit('passwordError');
            }
        });

        socket.on('addSimpleText', function (variable) {

            $('#text_' + pos).append("<span id='" + md5(variable.text) + "' style='color: " + texte_color + "; text-shadow: 1px 1px " + bg_color + "; background-color: rgba(0,0,0,0);'>" + variable.text + "<br></span>");

            io.sockets.emit('addtexte', {
                id: md5(variable.text),
                text: variable.text
            });

        });

        socket.on('delSimpleText', function (variable) {
            console.log('delSimpleText : ', variable);
            $('#' + variable.id).html('');
            io.sockets.emit('remTable', {
                id: variable.id
            });
        });

        socket.on('fullscreen', function () {
            win.toggleFullscreen();
        });

        socket.on('exit', function () {

            io.sockets.emit('close');

            win.on('close', function () {
                this.hide(); // Pretend to be closed already
                console.log("We're closing...");
                this.close(true); // then close it forcely
            });

            setTimeout(function () {
                win.close();
            }, 1000);

        });

    });

    //Écoute sur le port définit
    server.listen(47000);

    console.log("Server is listening in : " + ip.address() + ":" + 47000);
}
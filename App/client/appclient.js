//NE PAS MODIFIER : IP_DU_SERVER
//CETTE VARIABLE EST DYNAMIQUE LORS DE LA CRÉATION DU SERVER

const socket = io.connect('http://172.16.138.130:47000');

$(document).ready(function () {

    $('#login').show();
    $('#passwordError').hide();
    $('#app').hide();
    $('#navbar').hide();

    $('#loginform').submit(function (event) {
        event.preventDefault();
        socket.emit('login', {
            username: $('#username').val(),
            password: $('#password').val()
        })
    });

    $('#addTextForm').submit(function (event) {
        event.preventDefault();
        socket.emit('addSimpleText', {
            text: $('#simpleTexte').val()
        });
        $('#simpleTexte').val("");
    });

    socket.on('loginOk', function (info) {
        $('#passwordError').hide();
        $('#login').hide();
        $('#app').show();
        $('#navbar').show();
        $('#nav_user').html(info.username)
    });

    socket.on('passwordError', function () {
        $('#passwordError').addClass('alert-danger').removeClass('alert-success').show();
        $('#notif_strong').html("Ho non !");
        $('#notif_text').html("Erreur dans le mot de passe ou le nom d'utilisateur");

        setTimeout(function () {
            $('#passwordError').removeClass('alert-danger').removeClass('alert-success').hide();
        }, 2000);

    });

    socket.on('addtexte', function (info) {

        let body = '<tr id="' + info.id + '">' +
            '<th scope="row">' + info.text + '</th>' +
            '<td><button type="button" class="btn btn-danger" onclick="deltext(\'' + info.id + '\')">Supprimer</button></td>' +
            '</tr></div>';

        $('#listBody').append(body);
    });

    socket.on('remTable', function (info) {
        $('#' + info.id).html('')
    });

    socket.on('close', function () {
        $('#login').show();
        $('#app').hide();
        $('#navbar').hide();
        $('#passwordError').addClass('alert-danger').removeClass('alert-success').show();
        $('#notif_strong').html("Ho non !");
        $('#notif_text').html("L'application a été fermée");

        setTimeout(function () {
            $('#passwordError').removeClass('alert-danger').removeClass('alert-success').hide();
        }, 100000);
    });


});

function deltext(id_texte) {
    socket.emit('delSimpleText', {id: id_texte});
}

function fullscreen() {
    socket.emit('fullscreen');
}

function exit() {
    socket.emit('exit');
}